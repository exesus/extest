package ami

import (
	"exesus.com/extest/config"
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"log"
	"time"
)

const CentosProductCode = "aw0evgkw8e5c1q413zgy5pjce"

var CentosImageId string

func Configure() {
	CentosImageId = findCentosImageId()
}

func findCentosImageId() (centosImageId string) {
	imagesResp, err := config.Ec2Svc.DescribeImages(&ec2.DescribeImagesInput{
		Owners: aws.StringSlice([]string{"aws-marketplace"}),
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("architecture"),
				Values: aws.StringSlice([]string{"x86_64"}),
			},
			{
				Name:   aws.String("product-code"),
				Values: aws.StringSlice([]string{CentosProductCode}),
			},
			{
				Name:   aws.String("virtualization-type"),
				Values: aws.StringSlice([]string{"hvm"}),
			},
			{
				Name:   aws.String("state"),
				Values: aws.StringSlice([]string{"available"}),
			},
		},
	})
	util.HandleError(err)

	if imagesResp.Images == nil {
		log.Fatal("No CentOS images found")
	}

	log.Print("Found: ", len(imagesResp.Images), " CentOS images")

	var mostRecentCreationDate time.Time

	for _, image := range imagesResp.Images {
		log.Print("AMI: ", *image.ImageId, ", date: ", *image.CreationDate, ", name: ", *image.Name, ", desc: ", *image.Description)

		parsedCreationDate, err := time.Parse(time.RFC3339Nano, *image.CreationDate)
		util.HandleError(err)

		if len(centosImageId) < 1 || parsedCreationDate.After(mostRecentCreationDate) {
			centosImageId = *image.ImageId
			mostRecentCreationDate = parsedCreationDate
		}
	}

	if len(centosImageId) < 1 {
		log.Fatal("No CentOS image selected")
	}

	log.Print("AMI: ", centosImageId)

	return centosImageId
}
