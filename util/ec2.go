package util

import (
	"encoding/base64"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/iam"
	"log"
)

func FindSecurityGroupInVpc(ec2Svc *ec2.EC2, name string, vpcId string) (requestedSecurityGroup *ec2.SecurityGroup) {
	groupsResp, err := ec2Svc.DescribeSecurityGroups(&ec2.DescribeSecurityGroupsInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("vpc-id"),
				Values: aws.StringSlice([]string{vpcId}),
			},
			{
				Name:   aws.String("group-name"),
				Values: aws.StringSlice([]string{name}),
			},
		},
	})
	HandleError(err)

	if groupsResp != nil && groupsResp.SecurityGroups != nil {
		groupCount := len(groupsResp.SecurityGroups)

		if groupCount > 1 {
			log.Fatal("Found multiple security groups for name: ", name)
		} else if groupCount == 1 {
			securityGroup := groupsResp.SecurityGroups[0]

			if securityGroup.GroupId != nil && len(*securityGroup.GroupId) > 0 {
				requestedSecurityGroup = securityGroup
			}
		}
	}

	return requestedSecurityGroup
}

func CreateSecurityGroupInVpc(ec2Svc *ec2.EC2, name string, description string, vpcId string) (createdSecurityGroup *ec2.SecurityGroup) {
	log.Print("Creating security group: ", name)

	createSecurityGroupResp, err := ec2Svc.CreateSecurityGroup(&ec2.CreateSecurityGroupInput{
		GroupName:   aws.String(name),
		Description: aws.String(description),
		VpcId:       aws.String(vpcId),
	})
	HandleError(err)

	if createSecurityGroupResp.GroupId == nil {
		log.Fatal("No security group created")
	}

	createdSecurityGroupId := *createSecurityGroupResp.GroupId

	if len(createdSecurityGroupId) < 1 {
		log.Fatal("Empty security group ID returned")
	}

	createdSecurityGroup = &ec2.SecurityGroup{
		GroupId: aws.String(createdSecurityGroupId),
		VpcId:   aws.String(vpcId),
	}

	return createdSecurityGroup
}

func FindInstanceByName(ec2Svc *ec2.EC2, subnetId string, name string) (requestedInstance *ec2.Instance) {
	instancesResp, err := ec2Svc.DescribeInstances(&ec2.DescribeInstancesInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("network-interface.subnet-id"),
				Values: aws.StringSlice([]string{subnetId}),
			},
			{
				Name:   aws.String("tag:Name"),
				Values: aws.StringSlice([]string{name}),
			},
		},
	})
	HandleError(err)

	if instancesResp.Reservations != nil {
		reservationCount := len(instancesResp.Reservations)

		if reservationCount < 1 {
			log.Fatal("Empty reservations list")
		} else if reservationCount > 1 {
			log.Fatal("Found: ", reservationCount, " reservations for instance: ", name)
		}

		reservation := instancesResp.Reservations[0]

		if reservation.Instances != nil {
			for _, instance := range reservation.Instances {
				if instance.InstanceId != nil && instance.State != nil && instance.State.Name != nil && *instance.State.Name != "shutting-down" && *instance.State.Name != "terminated" {
					if requestedInstance != nil {
						log.Fatal("Found multiple instances for name: ", name)
					}

					requestedInstance = instance
				}
			}
		}
	}

	return requestedInstance
}

func CreateInstance(ec2Svc *ec2.EC2, name string, waitUntilRunning bool, iamInstanceProfile *iam.InstanceProfile, instanceType string, networkInterfaceSpecification *ec2.InstanceNetworkInterfaceSpecification, rootVolumeSize int64, keyName string, amiId string, userData string) (createdInstance *ec2.Instance) {
	if len(name) > 0 {
		log.Print("Creating instance: ", name)
	}

	var iamInstanceProfileSpec *ec2.IamInstanceProfileSpecification

	if iamInstanceProfile != nil {
		iamInstanceProfileSpec = &ec2.IamInstanceProfileSpecification{
			Arn: iamInstanceProfile.Arn,
		}
	}

	var encodedUserData *string

	if len(userData) > 0 {
		encodedUserData = aws.String(base64.StdEncoding.EncodeToString([]byte(userData)))
	}

	runInstancesResp, err := ec2Svc.RunInstances(&ec2.RunInstancesInput{
		ImageId:            aws.String(amiId),
		InstanceType:       aws.String(instanceType),
		IamInstanceProfile: iamInstanceProfileSpec,
		KeyName:            aws.String(keyName),
		MinCount:           aws.Int64(1),
		MaxCount:           aws.Int64(1),
		UserData:           encodedUserData,
		NetworkInterfaces:  []*ec2.InstanceNetworkInterfaceSpecification{networkInterfaceSpecification},
		BlockDeviceMappings: []*ec2.BlockDeviceMapping{
			{
				DeviceName: aws.String("/dev/sda1"),
				Ebs: &ec2.EbsBlockDevice{
					DeleteOnTermination: aws.Bool(true),
					VolumeSize:          aws.Int64(rootVolumeSize),
					VolumeType:          aws.String("gp2"),
				},
			},
		},
	})
	HandleError(err)

	if runInstancesResp.Instances == nil {
		log.Fatal("No instances created")
	}

	instanceCount := len(runInstancesResp.Instances)

	if instanceCount != 1 {
		log.Fatal("Created: ", instanceCount, " instances")
	}

	createdInstance = runInstancesResp.Instances[0]

	if createdInstance.InstanceId == nil {
		log.Fatal("Created instance has no ID")
	}

	if len(*createdInstance.InstanceId) < 1 {
		log.Fatal("Empty created instance ID")
	}

	err = ec2Svc.WaitUntilInstanceExists(&ec2.DescribeInstancesInput{
		InstanceIds: []*string{createdInstance.InstanceId},
	})
	HandleError(err)

	if len(name) > 0 {
		_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
			Resources: []*string{createdInstance.InstanceId},
			Tags: []*ec2.Tag{
				{
					Key:   aws.String("Name"),
					Value: aws.String(name),
				},
			},
		})
		HandleError(err)
	}

	if waitUntilRunning {
		err = ec2Svc.WaitUntilInstanceRunning(&ec2.DescribeInstancesInput{
			InstanceIds: []*string{createdInstance.InstanceId},
		})
		HandleError(err)
	}

	return createdInstance
}

func FindAddressByName(ec2Svc *ec2.EC2, name string) (requestedAddress *ec2.Address) {
	addressesResp, err := ec2Svc.DescribeAddresses(&ec2.DescribeAddressesInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("tag:Name"),
				Values: aws.StringSlice([]string{name}),
			},
		},
	})
	HandleError(err)

	if addressesResp.Addresses != nil {
		addressCount := len(addressesResp.Addresses)

		if addressCount < 1 {
			log.Fatal("Empty address list")
		} else if addressCount > 1 {
			log.Fatal("Found: ", addressCount, " addresses for name: ", name)
		}

		requestedAddress = addressesResp.Addresses[0]
	}

	return requestedAddress
}

func AllocateAddress(ec2Svc *ec2.EC2, name string) (allocatedAddress *ec2.Address) {
	log.Print("Allocating address: ", name)

	allocateAddressResp, err := ec2Svc.AllocateAddress(&ec2.AllocateAddressInput{
		Domain: aws.String("vpc"),
	})
	HandleError(err)

	if allocateAddressResp.AllocationId == nil {
		log.Fatal("No address allocated")
	}

	if allocateAddressResp.PublicIp == nil {
		log.Fatal("No public IP allocated")
	}

	if len(*allocateAddressResp.PublicIp) < 1 {
		log.Fatal("Allocated public IP was empty")
	}

	allocatedAddress = &ec2.Address{
		AllocationId:   allocateAddressResp.AllocationId,
		Domain:         allocateAddressResp.Domain,
		PublicIp:       allocateAddressResp.PublicIp,
		PublicIpv4Pool: allocateAddressResp.PublicIpv4Pool,
	}

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{allocatedAddress.AllocationId},
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})
	HandleError(err)

	return allocatedAddress
}

func AssociateAddressWithInstance(ec2Svc *ec2.EC2, address *ec2.Address, instance *ec2.Instance) {
	log.Print("Associating address: ", *address.PublicIp, " with instance: ", *instance.InstanceId)

	_, err := ec2Svc.AssociateAddress(&ec2.AssociateAddressInput{
		AllocationId:       address.AllocationId,
		InstanceId:         instance.InstanceId,
		AllowReassociation: aws.Bool(true),
	})
	HandleError(err)
}

func FindNetworkInterfaceByName(ec2Svc *ec2.EC2, name string) (requestedNetworkInterface *ec2.NetworkInterface) {
	interfacesResp, err := ec2Svc.DescribeNetworkInterfaces(&ec2.DescribeNetworkInterfacesInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("tag:Name"),
				Values: aws.StringSlice([]string{name}),
			},
		},
	})
	HandleError(err)

	if interfacesResp.NetworkInterfaces != nil {
		interfaceCount := len(interfacesResp.NetworkInterfaces)

		if interfaceCount < 1 {
			log.Fatal("Empty network interface list")
		} else if interfaceCount > 1 {
			log.Fatal("Found: ", interfaceCount, " network interfaces for name: ", name)
		}

		requestedNetworkInterface = interfacesResp.NetworkInterfaces[0]
	}

	return requestedNetworkInterface
}

func CreateNetworkInterface(ec2Svc *ec2.EC2, name string, description string, subnetId string, securityGroups []*ec2.SecurityGroup) (createdNetworkInterface *ec2.NetworkInterface) {
	log.Print("Allocating address: ", name)

	var securityGroupIds []*string

	if len(securityGroups) > 0 {
		for _, securityGroup := range securityGroups {
			securityGroupIds = append(securityGroupIds, securityGroup.GroupId)
		}
	}

	createResp, err := ec2Svc.CreateNetworkInterface(&ec2.CreateNetworkInterfaceInput{
		Description: aws.String(description),
		SubnetId:    aws.String(subnetId),
		Groups:      securityGroupIds,
	})
	HandleError(err)

	createdNetworkInterface = createResp.NetworkInterface

	if createdNetworkInterface == nil {
		log.Fatal("No network interface created")
	}

	if createdNetworkInterface.NetworkInterfaceId == nil {
		log.Fatal("No network interface ID")
	}

	if len(*createdNetworkInterface.NetworkInterfaceId) < 1 {
		log.Fatal("Network interface ID was empty")
	}

	if createdNetworkInterface.PrivateIpAddress == nil {
		log.Fatal("No private IP address")
	}

	if len(*createdNetworkInterface.PrivateIpAddress) < 1 {
		log.Fatal("Private IP address was empty")
	}

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{createdNetworkInterface.NetworkInterfaceId},
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})
	HandleError(err)

	return createdNetworkInterface
}

func AuthorizeIngressFromCidr(ec2Svc *ec2.EC2, securityGroup *ec2.SecurityGroup, cidrIp string, protocol string, fromPort int64, toPort int64) {
	if securityGroup.IpPermissions != nil {
		for _, ipPermission := range securityGroup.IpPermissions {
			if ipPermission.IpProtocol != nil && *ipPermission.IpProtocol == protocol && ipPermission.FromPort != nil && *ipPermission.FromPort == fromPort && ipPermission.ToPort != nil && *ipPermission.ToPort == toPort {
				if ipPermission.IpRanges != nil {
					for _, ipRange := range ipPermission.IpRanges {
						if ipRange.CidrIp != nil && *ipRange.CidrIp == cidrIp {
							return
						}
					}
				}
			}
		}
	}

	_, err := ec2Svc.AuthorizeSecurityGroupIngress(&ec2.AuthorizeSecurityGroupIngressInput{
		CidrIp:     aws.String(cidrIp),
		GroupId:    securityGroup.GroupId,
		IpProtocol: aws.String(protocol),
		FromPort:   aws.Int64(fromPort),
		ToPort:     aws.Int64(toPort),
	})
	HandleError(err)

	log.Print("Authorized ingress to group: ", *securityGroup.GroupId, " from CIDR: ", cidrIp)
}

func AuthorizeIngressFromSecurityGroup(ec2Svc *ec2.EC2, securityGroup *ec2.SecurityGroup, fromSecurityGroup *ec2.SecurityGroup, protocol string, fromPort int64, toPort int64) {
	if securityGroup.IpPermissions != nil {
		for _, ipPermission := range securityGroup.IpPermissions {
			if ipPermission.IpProtocol != nil && *ipPermission.IpProtocol == protocol && ipPermission.FromPort != nil && *ipPermission.FromPort == fromPort && ipPermission.ToPort != nil && *ipPermission.ToPort == toPort {
				if ipPermission.UserIdGroupPairs != nil {
					for _, userIdGroupPair := range ipPermission.UserIdGroupPairs {
						if userIdGroupPair.UserId != nil && userIdGroupPair.GroupId != nil && *userIdGroupPair.UserId == *fromSecurityGroup.OwnerId && *userIdGroupPair.GroupId == *fromSecurityGroup.GroupId {
							return
						}
					}
				}
			}
		}
	}

	_, err := ec2Svc.AuthorizeSecurityGroupIngress(&ec2.AuthorizeSecurityGroupIngressInput{
		IpPermissions: []*ec2.IpPermission{
			{
				UserIdGroupPairs: []*ec2.UserIdGroupPair{
					{
						UserId:  fromSecurityGroup.OwnerId,
						GroupId: fromSecurityGroup.GroupId,
					},
				},
				FromPort:   aws.Int64(fromPort),
				ToPort:     aws.Int64(toPort),
				IpProtocol: aws.String(protocol),
			},
		},
		GroupId: securityGroup.GroupId,
	})
	HandleError(err)

	log.Print("Authorized ingress to group: ", *securityGroup.GroupId, " from group: ", *fromSecurityGroup.GroupId)
}
