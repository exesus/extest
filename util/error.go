package util

import (
	"log"
)

func HandleError(err error) {
	if err != nil {
		log.Fatalf("ERROR [type: %T]: %s", err, err.Error())
	}
}
