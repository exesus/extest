package util

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/iam"
	"log"
	"net/url"
)

func FindIamRole(iamSvc *iam.IAM, pathPrefix string, name string) (requestedRole *iam.Role) {
	listResp, err := iamSvc.ListRoles(&iam.ListRolesInput{
		PathPrefix: aws.String(pathPrefix),
	})
	HandleError(err)

	if listResp.Roles != nil {
		if len(listResp.Roles) > 0 {
			for _, role := range listResp.Roles {
				if role.RoleName != nil && *role.RoleName == name {
					requestedRole = role
				}
			}
		}
	}

	return requestedRole
}

func CreateIamRole(iamSvc *iam.IAM, path string, name string, assumeRolePolicyDocument string) (createdRole *iam.Role) {
	createResp, err := iamSvc.CreateRole(&iam.CreateRoleInput{
		Path:                     aws.String(path),
		RoleName:                 aws.String(name),
		AssumeRolePolicyDocument: aws.String(assumeRolePolicyDocument),
	})
	HandleError(err)

	if createResp.Role == nil {
		log.Fatal("No role created")
	}

	createdRole = createResp.Role

	if createdRole.RoleId == nil {
		log.Fatal("No role ID returned")
	}

	if len(*createdRole.RoleId) < 1 {
		log.Fatal("Empty role ID returned")
	}

	return createdRole
}

func FindOrCreateIamRoleWithEC2AssumePolicy(iamSvc *iam.IAM, pathPrefix string, name string) (requestedRole *iam.Role) {
	requestedRole = FindIamRole(iamSvc, pathPrefix, name)

	if requestedRole == nil {
		requestedRole = CreateIamRole(iamSvc, pathPrefix, name, `{
			"Version": "2012-10-17",
			"Statement": [
				{
					"Effect": "Allow",
					"Action": "sts:AssumeRole",
					"Principal": {
						"Service": "ec2.amazonaws.com"
					}
				}
			]
		}`)
	}

	return requestedRole
}

func FindIamPolicy(iamSvc *iam.IAM, pathPrefix string, name string) (requestedPolicy *iam.Policy) {
	listResp, err := iamSvc.ListPolicies(&iam.ListPoliciesInput{
		PathPrefix: aws.String(pathPrefix),
	})
	HandleError(err)

	if listResp.Policies != nil {
		if len(listResp.Policies) > 0 {
			for _, policy := range listResp.Policies {
				if policy.PolicyName != nil && *policy.PolicyName == name {
					requestedPolicy = policy
				}
			}
		}
	}

	return requestedPolicy
}

func CreateIamPolicy(iamSvc *iam.IAM, path string, name string, policyDocument string) (createdPolicy *iam.Policy) {
	createResp, err := iamSvc.CreatePolicy(&iam.CreatePolicyInput{
		Path:           aws.String(path),
		PolicyName:     aws.String(name),
		PolicyDocument: aws.String(policyDocument),
	})
	HandleError(err)

	if createResp.Policy == nil {
		log.Fatal("No policy created")
	}

	createdPolicy = createResp.Policy

	if createdPolicy.PolicyId == nil {
		log.Fatal("No policy ID returned")
	}

	if len(*createdPolicy.PolicyId) < 1 {
		log.Fatal("Empty policy ID returned")
	}

	return createdPolicy
}

func FindOrIamCreatePolicy(iamSvc *iam.IAM, pathPrefix string, name string, policyDocument string) (requestedPolicy *iam.Policy) {
	requestedPolicy = FindIamPolicy(iamSvc, pathPrefix, name)

	if requestedPolicy == nil {
		requestedPolicy = CreateIamPolicy(iamSvc, pathPrefix, name, policyDocument)
	}

	return requestedPolicy
}

func EnsureIamPolicy(iamSvc *iam.IAM, pathPrefix string, name string, policyDocument string) (requestedPolicy *iam.Policy) {
	requestedPolicy = FindIamPolicy(iamSvc, pathPrefix, name)

	if requestedPolicy == nil {
		requestedPolicy = CreateIamPolicy(iamSvc, pathPrefix, name, policyDocument)
	} else {
		defaultPolicyResp, err := iamSvc.GetPolicyVersion(&iam.GetPolicyVersionInput{
			PolicyArn: requestedPolicy.Arn,
			VersionId: requestedPolicy.DefaultVersionId,
		})
		HandleError(err)

		if defaultPolicyResp.PolicyVersion == nil {
			log.Fatal("Default policy version is missing")
		}

		if defaultPolicyResp.PolicyVersion.Document == nil {
			log.Fatal("Default policy version's document is missing")
		}

		if len(*defaultPolicyResp.PolicyVersion.Document) < 1 {
			log.Fatal("Default policy version's document is empty")
		}

		unescapedDefaultVersion, err := url.QueryUnescape(*defaultPolicyResp.PolicyVersion.Document)
		HandleError(err)

		existingVersion := StripWhitespace(unescapedDefaultVersion)
		newVersion := StripWhitespace(policyDocument)

		if existingVersion != newVersion {
			log.Print("Existing policy version: ", existingVersion)
			log.Print("Creating policy version: ", newVersion)

			createVersionResp, err := iamSvc.CreatePolicyVersion(&iam.CreatePolicyVersionInput{
				PolicyArn:      requestedPolicy.Arn,
				PolicyDocument: aws.String(policyDocument),
				SetAsDefault:   aws.Bool(true),
			})
			HandleError(err)

			if createVersionResp.PolicyVersion == nil {
				log.Fatal("Created policy version is missing")
			}

			if createVersionResp.PolicyVersion.VersionId == nil {
				log.Fatal("Created policy version's ID is missing")
			}

			if len(*createVersionResp.PolicyVersion.VersionId) < 1 {
				log.Fatal("Created policy version's ID is empty")
			}
		}
	}

	return requestedPolicy
}

func FindIamRolePolicyAttachment(iamSvc *iam.IAM, pathPrefix string, roleName string, policyName string) (requestedAttachedPolicy *iam.AttachedPolicy) {
	listResp, err := iamSvc.ListAttachedRolePolicies(&iam.ListAttachedRolePoliciesInput{
		PathPrefix: aws.String(pathPrefix),
		RoleName:   aws.String(roleName),
	})
	HandleError(err)

	if listResp.AttachedPolicies != nil {
		if len(listResp.AttachedPolicies) > 0 {
			for _, attachedPolicy := range listResp.AttachedPolicies {
				if attachedPolicy.PolicyName != nil && *attachedPolicy.PolicyName == policyName {
					requestedAttachedPolicy = attachedPolicy
				}
			}
		}
	}

	return requestedAttachedPolicy
}

func AttachIamPolicyToRole(iamSvc *iam.IAM, policyArn string, roleName string) {
	_, err := iamSvc.AttachRolePolicy(&iam.AttachRolePolicyInput{
		PolicyArn: aws.String(policyArn),
		RoleName:  aws.String(roleName),
	})
	HandleError(err)
}

func EnsureIamPolicyAttachedToRole(iamSvc *iam.IAM, pathPrefix string, iamRole *iam.Role, iamPolicy *iam.Policy) {
	policyAttachment := FindIamRolePolicyAttachment(iamSvc, pathPrefix, *iamRole.RoleName, *iamPolicy.PolicyName)

	if policyAttachment == nil {
		AttachIamPolicyToRole(iamSvc, *iamPolicy.Arn, *iamRole.RoleName)
	}
}

func FindIamInstanceProfile(iamSvc *iam.IAM, pathPrefix string, profileName string) (requestedInstanceProfile *iam.InstanceProfile) {
	listResp, err := iamSvc.ListInstanceProfiles(&iam.ListInstanceProfilesInput{
		PathPrefix: aws.String(pathPrefix),
	})
	HandleError(err)

	if listResp.InstanceProfiles != nil {
		if len(listResp.InstanceProfiles) > 0 {
			for _, instanceProfile := range listResp.InstanceProfiles {
				if instanceProfile.InstanceProfileName != nil && *instanceProfile.InstanceProfileName == profileName {
					requestedInstanceProfile = instanceProfile
				}
			}
		}
	}

	return requestedInstanceProfile
}

func CreateIamInstanceProfile(iamSvc *iam.IAM, path string, profileName string) (createdInstanceProfile *iam.InstanceProfile) {
	createResp, err := iamSvc.CreateInstanceProfile(&iam.CreateInstanceProfileInput{
		Path:                aws.String(path),
		InstanceProfileName: aws.String(profileName),
	})
	HandleError(err)

	if createResp.InstanceProfile == nil {
		log.Fatal("No instance profile created")
	}

	createdInstanceProfile = createResp.InstanceProfile

	if createdInstanceProfile.InstanceProfileId == nil {
		log.Fatal("No instance profile ID returned")
	}

	if len(*createdInstanceProfile.InstanceProfileId) < 1 {
		log.Fatal("Empty instance profile ID returned")
	}

	return createdInstanceProfile
}

func AssociateIamInstanceProfileWithRole(iamSvc *iam.IAM, instanceProfile *iam.InstanceProfile, iamRole *iam.Role) {
	if instanceProfile.Roles != nil {
		for _, role := range instanceProfile.Roles {
			if role.RoleId != nil && *role.RoleId == *iamRole.RoleId {
				return
			}
		}
	}

	_, err := iamSvc.AddRoleToInstanceProfile(&iam.AddRoleToInstanceProfileInput{
		InstanceProfileName: instanceProfile.InstanceProfileName,
		RoleName:            iamRole.RoleName,
	})
	HandleError(err)
}

func FindOrCreateIamInstanceProfileWithRoleAssociation(iamSvc *iam.IAM, pathPrefix string, instanceProfileName string, role *iam.Role) (requestedInstanceProfile *iam.InstanceProfile) {
	requestedInstanceProfile = FindIamInstanceProfile(iamSvc, pathPrefix, instanceProfileName)

	if requestedInstanceProfile == nil {
		requestedInstanceProfile = CreateIamInstanceProfile(iamSvc, pathPrefix, instanceProfileName)
	}

	AssociateIamInstanceProfileWithRole(iamSvc, requestedInstanceProfile, role)

	return requestedInstanceProfile
}
