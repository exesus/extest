package util

import (
	"crypto/rand"
	"log"
	"strings"
	"unicode"
)

// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
func SecureRandomString(availableCharBytes string, length int) string {
	availableCharLength := len(availableCharBytes)

	if availableCharLength == 0 || availableCharLength > 256 {
		log.Fatal("availableCharBytes length must be greater than 0 and less than or equal to 256")
	}

	var bitLength byte
	var bitMask byte
	for bits := availableCharLength - 1; bits != 0; {
		bits = bits >> 1
		bitLength++
	}

	bitMask = 1<<bitLength - 1
	bufferSize := length + length/3
	result := make([]byte, length)

	for i, j, randomBytes := 0, 0, []byte{}; i < length; j++ {
		if j%bufferSize == 0 {
			randomBytes = SecureRandomBytes(bufferSize)
		}

		if idx := int(randomBytes[j%length] & bitMask); idx < availableCharLength {
			result[i] = availableCharBytes[idx]
			i++
		}
	}

	return string(result)
}

func SecureRandomBytes(length int) []byte {
	var randomBytes = make([]byte, length)
	_, err := rand.Read(randomBytes)
	HandleError(err)

	return randomBytes
}

// https://stackoverflow.com/questions/32081808/strip-all-whitespace-from-a-string
func StripWhitespace(str string) string {
	var b strings.Builder

	b.Grow(len(str))

	for _, ch := range str {
		if !unicode.IsSpace(ch) {
			b.WriteRune(ch)
		}
	}

	return b.String()
}
