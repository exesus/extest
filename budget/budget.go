package budget

import (
	"exesus.com/extest/config"
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/budgets"
	"log"
)

func Configure() {
	budgetResp, err := config.BudgetSvc.DescribeBudgets(&budgets.DescribeBudgetsInput{
		AccountId: aws.String(config.AwsAccount),
	})
	util.HandleError(err)

	if len(budgetResp.Budgets) < 1 {
		log.Fatal("No budgets found")
	}

	log.Print("Found: ", len(budgetResp.Budgets), " budgets")

	for _, budget := range budgetResp.Budgets {
		log.Print("Budget: ", *budget.BudgetName, ", type: ", *budget.BudgetType)
		calculated := budget.CalculatedSpend

		if calculated == nil {
			log.Fatal("No calculated spend available")
		}

		actual := calculated.ActualSpend

		if actual == nil {
			log.Print("No actual spend calculated")
		} else {
			log.Print("Actual amount: ", *actual.Amount, ", unit: ", *actual.Unit)
		}

		forecast := calculated.ForecastedSpend

		if forecast == nil {
			log.Print("No forecasted spend calculated")
		} else {
			log.Print("Forecasted amount: ", *forecast.Amount, ", unit: ", *forecast.Unit)
		}
	}
}
