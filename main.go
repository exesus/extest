package main

import (
	"exesus.com/extest/ami"
	"exesus.com/extest/bastion"
	"exesus.com/extest/budget"
	"exesus.com/extest/config"
	"exesus.com/extest/consul"
	"exesus.com/extest/proxy"
	"exesus.com/extest/vault"
	"exesus.com/extest/vpc"
	"log"
)

func main() {
	log.Println("Initializing config")
	config.Configure()

	log.Println("Configuring budget")
	budget.Configure()

	log.Println("Configuring VPC")
	vpc.Configure()

	log.Println("Configuring AMIs")
	ami.Configure()

	log.Println("Configuring bastion")
	bastion.Configure()

	log.Println("Configuring proxy")
	proxy.Configure()

	log.Println("Configuring Consul")
	consul.Configure()

	log.Println("Configuring Vault")
	vault.Configure()
}
