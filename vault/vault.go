package vault

import (
	"bytes"
	"exesus.com/extest/ami"
	"exesus.com/extest/config"
	"exesus.com/extest/consul"
	"exesus.com/extest/proxy"
	"exesus.com/extest/util"
	"exesus.com/extest/vpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/kms"
	"log"
	"text/template"
)

const IamPolicyName = "VaultViaConsul"
const IamInstanceProfileName = "VaultServerProfile"
const InstanceType = "t2.medium"
const InstanceNameZoneA = "EXVault01"
const KeyAlias = "VaultUnsealKey-" + config.EnvironmentName
const KeyDescription = "Vault unseal key for environment: " + config.EnvironmentName
const Version = "1.0.1"

const s3RecoveryKeyPath = "/vault/keys/recovery"

var KeyId string

func Configure() {
	keyResp, err := config.KmsSvc.DescribeKey(&kms.DescribeKeyInput{
		KeyId: aws.String("alias/" + KeyAlias),
	})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == kms.ErrCodeNotFoundException {
			// ignore
		} else {
			util.HandleError(err)
		}
	}

	if keyResp.KeyMetadata != nil && keyResp.KeyMetadata.KeyId != nil && len(*keyResp.KeyMetadata.KeyId) > 0 {
		KeyId = *keyResp.KeyMetadata.KeyId

		log.Print("Found KMS key: ", KeyId)
	} else {
		log.Print("No KMS key, creating one")

		createKeyResp, err := config.KmsSvc.CreateKey(&kms.CreateKeyInput{
			Description: aws.String(KeyDescription),
		})
		util.HandleError(err)

		if createKeyResp.KeyMetadata == nil {
			log.Fatal("Created key had no metadata")
		}

		if createKeyResp.KeyMetadata.KeyId == nil {
			log.Fatal("Created key had no key ID")
		}

		if len(*createKeyResp.KeyMetadata.KeyId) < 1 {
			log.Fatal("Created key had an empty key ID")
		}

		_, err = config.KmsSvc.CreateAlias(&kms.CreateAliasInput{
			AliasName:   aws.String("alias/" + KeyAlias),
			TargetKeyId: createKeyResp.KeyMetadata.KeyId,
		})
		util.HandleError(err)

		KeyId = *createKeyResp.KeyMetadata.KeyId

		log.Print("Created KMS key: ", KeyId)
	}

	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8200, 8200)
	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8201, 8201)

	policyVars := struct {
		AwsAccount   string
		AwsPartition string
		AwsRegion    string
		BucketName   string
	}{
		AwsAccount:   config.AwsAccount,
		AwsPartition: config.AwsPartition,
		AwsRegion:    config.AwsRegion,
		BucketName:   *config.Bucket.Name,
	}
	policyTemplate, err := template.New("VaultIamPolicy").Parse(`{
		"Version": "2012-10-17",
		"Statement": [
			{
				"Effect": "Allow",
				"Action": "ec2:DescribeInstances",
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:RequestedRegion": "{{.AwsRegion}}"
					}
				}
			},
			{
				"Effect": "Allow",
				"Action": [
					"kms:Encrypt",
					"kms:Decrypt",
					"kms:DescribeKey"
				],
				"Resource": "*"
			},
			{
				"Effect":"Allow",
					"Action": [
						"s3:PutObject",
						"s3:GetObject"
					],
					"Resource":"arn:{{.AwsPartition}}:s3:::{{.BucketName}}/*"
			},
			{
				"Effect": "Allow",
				"Action": [
					"iam:AttachUserPolicy",
					"iam:CreateAccessKey",
					"iam:CreateUser",
					"iam:DeleteAccessKey",
					"iam:DeleteUser",
					"iam:DeleteUserPolicy",
					"iam:DetachUserPolicy",
					"iam:ListAccessKeys",
					"iam:ListAttachedUserPolicies",
					"iam:ListGroupsForUser",
					"iam:ListUserPolicies",
					"iam:PutUserPolicy",
					"iam:RemoveUserFromGroup"
				],
				"Resource": [
					"arn:{{.AwsPartition}}:iam::{{.AwsAccount}}:user/vault-*"
				]
			}
		]
	}`)
	util.HandleError(err)

	var policyBytes bytes.Buffer

	err = policyTemplate.Execute(&policyBytes, policyVars)
	util.HandleError(err)

	iamPolicy := util.EnsureIamPolicy(config.IamSvc, config.IamPathPrefix, IamPolicyName, policyBytes.String())

	util.EnsureIamPolicyAttachedToRole(config.IamSvc, config.IamPathPrefix, config.VaultServerRole, iamPolicy)

	iamInstanceProfile := util.FindOrCreateIamInstanceProfileWithRoleAssociation(config.IamSvc, config.IamPathPrefix, IamInstanceProfileName, config.VaultServerRole)
	aZoneInstance := util.FindInstanceByName(config.Ec2Svc, vpc.AppZoneASubnetId, InstanceNameZoneA)

	if aZoneInstance == nil {
		userDataVars := struct {
			AgentToken        string
			BucketName        string
			ConsulVersion     string
			EnvironmentName   string
			KmsKeyId          string
			NoProxy           string
			ProxyAddr         string
			RetryJoinTag      string
			S3RecoveryKeyPath string
			VaultVersion      string
		}{
			AgentToken:        consul.MasterToken,
			BucketName:        *config.Bucket.Name,
			ConsulVersion:     consul.Version,
			EnvironmentName:   config.EnvironmentName,
			KmsKeyId:          KeyId,
			NoProxy:           "169.254.169.254",
			ProxyAddr:         *proxy.NetworkInterface.PrivateIpAddress,
			RetryJoinTag:      consul.RetryJoinTagName,
			S3RecoveryKeyPath: s3RecoveryKeyPath,
			VaultVersion:      Version,
		}
		userDataTemplate, err := template.New("VaultUserData").Parse(`#!/bin/bash
			proxy_available=0
			attempts=0
			while [ "${proxy_available}" = "0" ];
			do
				(timeout 1 bash -c "echo > /dev/tcp/{{.ProxyAddr}}/3128") >/dev/null 2>&1
				result=$?
				attempts=$((attempts+1))

				if [ "${result}" = "0" ];
				then
					echo "Proxy port open on attempt ${attempts}"
					proxy_available=1
				elif [ "${attempts}" -lt 90 ];
				then
					>&2 echo "Proxy port not yet open on attempt ${attempts} - sleeping"
					sleep 5
				else
					>&2 echo "Proxy port not open after ${attempts} attempts - giving up"
					exit 1
				fi
			done
			ip=$(ip route get 1 | awk '{print $NF;exit}')
			grep "{{.ProxyAddr}}" /etc/environment || tee -a /etc/environment > /dev/null << __EOF__
http_proxy=http://{{.ProxyAddr}}:3128/
https_proxy=http://{{.ProxyAddr}}:3128/
no_proxy={{.NoProxy}},${ip}
__EOF__
			[ -f /etc/profile.d/proxy.sh ] || tee /etc/profile.d/proxy.sh > /dev/null << __EOF__
http_proxy=http://{{.ProxyAddr}}:3128/
https_proxy=http://{{.ProxyAddr}}:3128/
no_proxy={{.NoProxy}},${ip}
export http_proxy https_proxy no_proxy
__EOF__
			. /etc/profile.d/proxy.sh

			mkdir -p /usr/local/bin

			curl https://releases.hashicorp.com/consul/{{.ConsulVersion}}/consul_{{.ConsulVersion}}_linux_amd64.zip > /root/consul_{{.ConsulVersion}}.zip
			python -c 'from zipfile import ZipFile; zf = ZipFile("/root/consul_{{.ConsulVersion}}.zip", "r"); zf.extractall("/root"); zf.close()'
			mv -f /root/consul /usr/local/bin/consul-{{.ConsulVersion}}
			chmod a+x /usr/local/bin/consul-{{.ConsulVersion}}
			chown root:root /usr/local/bin/consul-{{.ConsulVersion}}
			ln -sf /usr/local/bin/consul-{{.ConsulVersion}} /usr/local/bin/consul
			useradd consul
			rm -rf /opt/consul
			mkdir -p /opt/consul
			chown -R consul:consul /opt/consul
			mkdir -p /etc/consul
			touch /etc/consul/config.json
			chown -R consul:consul /etc/consul
			chmod 0700 /etc/consul
			chown 0600 /etc/consul/config.json
			tee /etc/consul/config.json > /dev/null << __EOF__
{
  "acl": {
    "enabled": true,
    "default_policy": "deny",
    "down_policy": "deny",
    "tokens": {
      "default": "{{.AgentToken}}"
    }
  },
  "datacenter": "{{.EnvironmentName}}",
  "data_dir": "/opt/consul",
  "disable_remote_exec": true,
  "disable_update_check": true,
  "leave_on_terminate": true,
  "log_level": "INFO",
  "retry_join": [
    "provider=aws tag_key={{.RetryJoinTag}} tag_value={{.EnvironmentName}}"
  ]
}
__EOF__
			tee /etc/systemd/system/consul.service > /dev/null << __EOF__
[Unit]
Description="HashiCorp Consul"
Documentation=https://www.consul.io/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/consul/config.json

[Service]
Environment=http_proxy=http://{{.ProxyAddr}}:3128/
Environment=https_proxy=http://{{.ProxyAddr}}:3128/
Environment=no_proxy={{.NoProxy}},${ip}
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent -config-dir=/etc/consul/
ExecReload=/usr/local/bin/consul reload
KillMode=process
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
__EOF__
			systemctl enable consul
			systemctl start consul

			curl https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/o/oniguruma-5.9.5-3.el7.x86_64.rpm > /root/oniguruma-5.9.5-3.el7.x86_64.rpm
			rpm -i /root/oniguruma-5.9.5-3.el7.x86_64.rpm
			curl https://dl.fedoraproject.org/pub/epel/7/x86_64/Packages/j/jq-1.5-1.el7.x86_64.rpm > /root/jq-1.5-1.el7.x86_64.rpm
			rpm -i /root/jq-1.5-1.el7.x86_64.rpm

			creds_available=0
			attempts=0
			while [ "${creds_available}" = "0" ];
			do
				[ -z "${aws_region}" ] && aws_region="$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r '.region')"
				[ ! -z "${aws_region}" ] && instance_profile="$(curl -s http://169.254.169.254/latest/meta-data/iam/security-credentials/)"
				[ ! -z "${instance_profile}" ] && aws_access_key_id="$(curl -s http://169.254.169.254/latest/meta-data/iam/security-credentials/${instance_profile} | jq -r '.AccessKeyId')"
				[ ! -z "${aws_access_key_id}" ] && aws_secret_access_key="$(curl -s http://169.254.169.254/latest/meta-data/iam/security-credentials/${instance_profile} | jq -r '.SecretAccessKey')"
				[ ! -z "${aws_secret_access_key}" ] && aws_security_token="$(curl -s http://169.254.169.254/latest/meta-data/iam/security-credentials/${instance_profile} | jq -r '.Token')"

				attempts=$((attempts+1))

				if [ ! -z "${aws_security_token}" ];
				then
					echo "Creds available on attempt ${attempts}"
					creds_available=1
				elif [ "${attempts}" -lt 90 ];
				then
					>&2 echo "Creds not yet available on attempt ${attempts} - sleeping"
					sleep 5
				else
					>&2 echo "Creds not available after ${attempts} attempts - giving up"
					exit 1
				fi
			done

			curl https://releases.hashicorp.com/vault/{{.VaultVersion}}/vault_{{.VaultVersion}}_linux_amd64.zip > /root/vault_{{.VaultVersion}}.zip
			python -c 'from zipfile import ZipFile; zf = ZipFile("/root/vault_{{.VaultVersion}}.zip", "r"); zf.extractall("/root"); zf.close()'
			mv -f /root/vault /usr/local/bin/vault-{{.VaultVersion}}
			chmod a+x /usr/local/bin/vault-{{.VaultVersion}}
			chown root:root /usr/local/bin/vault-{{.VaultVersion}}
			setcap 'cap_ipc_lock=+ep' /usr/local/bin/vault-{{.VaultVersion}}
			ln -sf /usr/local/bin/vault-{{.VaultVersion}} /usr/local/bin/vault
			useradd vault
			mkdir -p /etc/vault
			touch /etc/vault/config.hcl.tmpl
			chown -R vault:vault /etc/vault
			chmod 0700 /etc/vault
			chmod 0600 /etc/vault/config.hcl.tmpl
			tee /etc/vault/config.hcl.tmpl > /dev/null << __EOF__
storage "consul" {
  address = "127.0.0.1:8500"
  path    = "vault/"
}

listener "tcp" {
 address     = "__PRIVATE_IP__:8200"
 tls_disable = 1
}

seal "awskms" {
  region     = "${aws_region}"
  kms_key_id = "{{.KmsKeyId}}"
}
__EOF__

			tee /etc/systemd/system/vault.service > /dev/null << __EOF__
[Unit]
Description=HashiCorp Vault
Documentation=https://www.vaultproject.io/
Requires=consul.service
After=consul.service
ConditionFileNotEmpty=/etc/vault/config.hcl

[Service]
Environment=http_proxy=http://{{.ProxyAddr}}:3128/
Environment=https_proxy=http://{{.ProxyAddr}}:3128/
Environment=no_proxy={{.NoProxy}},${ip}
User=vault
Group=vault
PermissionsStartOnly=true
ExecStart=/usr/local/bin/vault server -config /etc/vault/config.hcl
ExecReload=/bin/kill -HUP \$MAINPID
KillSignal=SIGINT
Restart=on-failure

[Install]
WantedBy=multi-user.target
__EOF__
			touch /etc/vault/config.hcl
			chown vault:vault /etc/vault/config.hcl
			chmod 0600 /etc/vault/config.hcl
			cat /etc/vault/config.hcl.tmpl | sed "s/__PRIVATE_IP__/${ip}/g" > /etc/vault/config.hcl
			systemctl enable vault
			systemctl start vault
			s3_host="{{.BucketName}}.s3-${aws_region}.amazonaws.com"
			s3_path="{{.S3RecoveryKeyPath}}"
			recovery_key=""
			root_token=""
			vault_available=0
			attempts=0
			while [ "${vault_available}" = "0" ];
			do
				(timeout 1 bash -c "VAULT_ADDR=http://${ip}:8200 vault operator init -status") >/dev/null 2>&1
				result=$?
				attempts=$((attempts+1))

				if [ "${result}" = "0" ];
				then
					echo "Vault initialized as of attempt ${attempts}"
					echo "Vault initialized as of attempt ${attempts}" >> /var/log/vault.init
					vault_available=1
				elif [ "${result}" = "2" ];
				then
					>&2 echo "Initializing Vault on attempt ${attempts}"
					echo "Initializing Vault on attempt ${attempts}" >> /var/log/vault.init
					init_result=$(curl -X PUT -d '{"secret_shares":1,"secret_threshold":1,"recovery_shares":1,"recovery_threshold":1}' http://${ip}:8200/v1/sys/init)
					echo "Result: ${init_result}" >> /var/log/vault.init
					recovery_key=$(echo "${init_result}" | jq -r '.recovery_keys[0]')
					root_token=$(echo "${init_result}" | jq -r '.root_token')
					temp_file="$(mktemp)"
					trap "rm -f ${temp_file}" INT QUIT TERM EXIT
					echo -n "${recovery_key}" > "${temp_file}"
					content_length="${#recovery_key}"
					content_type="text/plain"
					content_md5="$(openssl md5 -binary < "${temp_file}" | base64)"
					date="$(date +'%a, %d %b %Y %H:%M:%S %z')"
					payload="$(printf "PUT\n${content_md5}\n${content_type}\n${date}\nx-amz-security-token:${aws_security_token}\nx-amz-server-side-encryption:AES256\n/{{.BucketName}}${s3_path}")"
					signature="$(echo -n "${payload}" | openssl sha1 -binary -hmac "${aws_secret_access_key}" | base64)"
					s3_cmd=("curl" "-v")
					s3_cmd+=("-X" "PUT")
					s3_cmd+=("-T" "${temp_file}")
					s3_cmd+=("-H" "Authorization: AWS ${aws_access_key_id}:${signature}")
					s3_cmd+=("-H" "Date: ${date}")
					s3_cmd+=("-H" "X-AMZ-Security-Token: ${aws_security_token}")
					s3_cmd+=("-H" "X-AMZ-Server-Side-Encryption: AES256")
					s3_cmd+=("-H" "Content-Length: ${content_length}")
					s3_cmd+=("-H" "Content-MD5: ${content_md5}")
					s3_cmd+=("-H" "Content-Type: ${content_type}")
					"${s3_cmd[@]}" "https://${s3_host}${s3_path}"
					sleep 5
				elif [ "${attempts}" -lt 90 ];
				then
					>&2 echo "Vault not yet initialized on attempt ${attempts} - sleeping"
					echo "Vault not yet initialized on attempt ${attempts} - sleeping" >> /var/log/vault.init
					sleep 5
				else
					>&2 echo "Vault not initialized after ${attempts} attempts - giving up"
					echo "Vault not initialized after ${attempts} attempts - giving up" >> /var/log/vault.init
					exit 1
				fi
			done
			[ ! -z "${recovery_key}" ] && echo "Recovery key: ${recovery_key}" >> /var/log/vault.init
			[ ! -z "${root_token}" ] && echo "Root token: ${root_token}" >> /var/log/vault.init
		`)
		util.HandleError(err)

		var userDataBytes bytes.Buffer

		err = userDataTemplate.Execute(&userDataBytes, userDataVars)
		util.HandleError(err)

		aZoneInstance = util.CreateInstance(
			config.Ec2Svc, InstanceNameZoneA, false, iamInstanceProfile, InstanceType,
			&ec2.InstanceNetworkInterfaceSpecification{
				AssociatePublicIpAddress: aws.Bool(false),
				DeleteOnTermination:      aws.Bool(true),
				SubnetId:                 aws.String(vpc.AppZoneASubnetId),
				Groups:                   []*string{vpc.AppSecurityGroup.GroupId},
				DeviceIndex:              aws.Int64(0),
			},
			20, vpc.DefaultKeyName, ami.CentosImageId,
			userDataBytes.String(),
		)

		_, err = config.Ec2Svc.CreateTags(&ec2.CreateTagsInput{
			Resources: []*string{aZoneInstance.InstanceId},
			Tags: []*ec2.Tag{
				{
					Key:   aws.String(consul.RetryJoinTagName),
					Value: aws.String(config.EnvironmentName),
				},
			},
		})
		util.HandleError(err)
	}
}
