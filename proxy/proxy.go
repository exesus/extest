package proxy

import (
	"exesus.com/extest/ami"
	"exesus.com/extest/bastion"
	"exesus.com/extest/config"
	"exesus.com/extest/util"
	"exesus.com/extest/vpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"strings"
)

const AddressName = "EXProxyAddr"
const NetworkInterfaceName = "EXProxyNIC"
const NetworkInterfaceDesc = "Proxy network interface"
const InstanceName = "EXProxy01"
const InstanceType = "t2.micro"
const SecurityGroupName = "EXProxy"
const SecurityGroupDesc = "Allows access to proxy servers"

var Address *ec2.Address
var NetworkInterface *ec2.NetworkInterface
var SecurityGroup *ec2.SecurityGroup

func Configure() {
	SecurityGroup = util.FindSecurityGroupInVpc(config.Ec2Svc, SecurityGroupName, vpc.UtilVpcId)

	if SecurityGroup == nil {
		SecurityGroup = util.CreateSecurityGroupInVpc(config.Ec2Svc, SecurityGroupName, SecurityGroupDesc, vpc.UtilVpcId)
	}

	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, SecurityGroup, bastion.SecurityGroup, "tcp", 22, 22)
	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, SecurityGroup, vpc.AppSecurityGroup, "tcp", 3128, 3128)

	Address = util.FindAddressByName(config.Ec2Svc, AddressName)

	if Address == nil {
		Address = util.AllocateAddress(config.Ec2Svc, AddressName)
	}

	NetworkInterface = util.FindNetworkInterfaceByName(config.Ec2Svc, NetworkInterfaceName)

	if NetworkInterface == nil {
		NetworkInterface = util.CreateNetworkInterface(config.Ec2Svc, NetworkInterfaceName, NetworkInterfaceDesc, vpc.ProxyZoneASubnetId, []*ec2.SecurityGroup{SecurityGroup})
	}

	instance := util.FindInstanceByName(config.Ec2Svc, vpc.ProxyZoneASubnetId, InstanceName)

	if instance == nil {
		instance = util.CreateInstance(
			config.Ec2Svc, InstanceName, true, nil, InstanceType, &ec2.InstanceNetworkInterfaceSpecification{
				NetworkInterfaceId:  NetworkInterface.NetworkInterfaceId,
				DeleteOnTermination: aws.Bool(false),
				DeviceIndex:         aws.Int64(0),
			},
			20, vpc.DefaultKeyName, ami.CentosImageId,
			strings.Replace(`#!/bin/bash
				yum -y install squid
				tee /etc/squid/squid.conf > /dev/null << __EOF__
http_port 3128

acl whitelist dstdomain dl.fedoraproject.org
acl whitelist dstdomain releases.hashicorp.com
acl whitelist dstdomain __CONFIG_S3_HOST__
acl whitelist dstdomain ec2.us-west-2.amazonaws.com
acl whitelist dstdomain kms.us-west-2.amazonaws.com
acl whitelist dstdomain aws.amazonaws.com
http_access allow whitelist

http_access deny all
__EOF__
				systemctl restart squid
			`, "__CONFIG_S3_HOST__", *config.Bucket.Name+".s3-"+config.AwsRegion+".amazonaws.com", -1),
		)
	}

	if Address.InstanceId == nil || len(*Address.InstanceId) < 1 || *Address.InstanceId != *instance.InstanceId {
		util.AssociateAddressWithInstance(config.Ec2Svc, Address, instance)
	}
}
