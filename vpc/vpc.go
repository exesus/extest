package vpc

import (
	"exesus.com/extest/config"
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"log"
	"time"
)

const DefaultKeyName = "exodefault"
const UtilVpcName = "EXUtil"
const UtilVpcCidrBlock = "192.168.0.0/16"
const BastionSubnetName = "EXBastion"
const BastionCidrBlock = "192.168.0.0/28"
const ProxySubnetName = "EXProxy"
const ProxyCidrBlock = "192.168.0.16/28"
const AppVpcName = "EXApp"
const AppVpcCidrBlock = "10.0.0.0/16"
const AppSubnetNameA = "EXAppZoneA"
const AppCidrBlockA = "10.0.0.0/18"
const AppSecurityGroupName = "EXApplication"
const AppSecurityGroupDesc = "Allows access to application servers"

var AZoneId string
var UtilVpcId string
var AppVpcId string
var AppZoneASubnetId string
var AppSecurityGroup *ec2.SecurityGroup
var BastionZoneASubnetId string
var ProxyZoneASubnetId string

func Configure() {
	AZoneId, _, _ = findAvailabilityZones(config.Ec2Svc)

	if len(AZoneId) < 1 {
		log.Fatal("Availability zone A not found or not available")
	}

	_, UtilVpcId, AppVpcId = findVpcIds(config.Ec2Svc, UtilVpcName, AppVpcName)

	if len(UtilVpcId) < 1 {
		UtilVpcId = createVpc(config.Ec2Svc, UtilVpcName, UtilVpcCidrBlock)
	}

	if len(AppVpcId) < 1 {
		AppVpcId = createVpc(config.Ec2Svc, AppVpcName, AppVpcCidrBlock)
	}

	vpcPeeringConnection := peerOwnedVpcs(config.Ec2Svc, AppVpcId, UtilVpcId)
	utilInternetGateway := findInternetGatewayForVpc(config.Ec2Svc, UtilVpcName)

	if utilInternetGateway == nil {
		utilInternetGateway = createInternetGateway(config.Ec2Svc, UtilVpcName)
	}

	associateInternetGatewayWithVpc(config.Ec2Svc, utilInternetGateway, UtilVpcId)

	utilRouteTable := findRouteTableForVpc(config.Ec2Svc, UtilVpcId)
	appRouteTable := findRouteTableForVpc(config.Ec2Svc, AppVpcId)

	routeToInternetGateway(config.Ec2Svc, utilRouteTable, utilInternetGateway, "0.0.0.0/0")
	routeToVpcPeeringConnection(config.Ec2Svc, utilRouteTable, vpcPeeringConnection, AppVpcCidrBlock)
	routeToVpcPeeringConnection(config.Ec2Svc, appRouteTable, vpcPeeringConnection, UtilVpcCidrBlock)

	BastionZoneASubnetId, _, _ = findSubnetsInVpc(config.Ec2Svc, UtilVpcId, BastionSubnetName, "", "")

	if len(BastionZoneASubnetId) < 1 {
		BastionZoneASubnetId = createSubnet(config.Ec2Svc, BastionSubnetName, BastionCidrBlock, AZoneId, UtilVpcId)
	}

	associateSubnetWithRouteTable(config.Ec2Svc, BastionZoneASubnetId, utilRouteTable)

	ProxyZoneASubnetId, _, _ = findSubnetsInVpc(config.Ec2Svc, UtilVpcId, ProxySubnetName, "", "")

	if len(ProxyZoneASubnetId) < 1 {
		ProxyZoneASubnetId = createSubnet(config.Ec2Svc, ProxySubnetName, ProxyCidrBlock, AZoneId, UtilVpcId)
	}

	associateSubnetWithRouteTable(config.Ec2Svc, ProxyZoneASubnetId, utilRouteTable)

	AppZoneASubnetId, _, _ = findSubnetsInVpc(config.Ec2Svc, AppVpcId, AppSubnetNameA, "", "")

	if len(AppZoneASubnetId) < 1 {
		AppZoneASubnetId = createSubnet(config.Ec2Svc, AppSubnetNameA, AppCidrBlockA, AZoneId, AppVpcId)
	}

	associateSubnetWithRouteTable(config.Ec2Svc, AppZoneASubnetId, appRouteTable)

	AppSecurityGroup = util.FindSecurityGroupInVpc(config.Ec2Svc, AppSecurityGroupName, AppVpcId)

	if AppSecurityGroup == nil {
		AppSecurityGroup = util.CreateSecurityGroupInVpc(config.Ec2Svc, AppSecurityGroupName, AppSecurityGroupDesc, AppVpcId)
	}
}

func findAvailabilityZones(ec2Svc *ec2.EC2) (aZoneId string, bZoneId string, cZoneId string) {
	azsResp, err := ec2Svc.DescribeAvailabilityZones(&ec2.DescribeAvailabilityZonesInput{})
	util.HandleError(err)

	if azsResp.AvailabilityZones == nil {
		log.Fatal("No availability zones found")
	}

	log.Print("Found ", len(azsResp.AvailabilityZones), " availability zones")

	for _, az := range azsResp.AvailabilityZones {
		if az.RegionName != nil && az.ZoneId != nil && az.ZoneName != nil && az.State != nil {
			log.Print("Region: ", *az.RegionName, ", AZ: ", *az.ZoneId, ", name: ", *az.ZoneName, ", state: ", *az.State)

			if *az.State == "available" {
				if *az.ZoneName == (*az.RegionName + "a") {
					aZoneId = *az.ZoneId
				} else if *az.ZoneName == (*az.RegionName + "b") {
					bZoneId = *az.ZoneId
				} else if *az.ZoneName == (*az.RegionName + "c") {
					cZoneId = *az.ZoneId
				}
			}
		}
	}

	return aZoneId, bZoneId, cZoneId
}

func findVpcIds(ec2Svc *ec2.EC2, name1 string, name2 string) (defaultVpcId string, vpc1Id string, vpc2Id string) {
	name1Len := len(name1)
	name2Len := len(name2)
	vpcsResp, err := ec2Svc.DescribeVpcs(&ec2.DescribeVpcsInput{})
	util.HandleError(err)

	if vpcsResp.Vpcs == nil {
		log.Print("No vpcs found")
	} else {
		log.Print("Found: ", len(vpcsResp.Vpcs), " vpcs")

		for _, vpc := range vpcsResp.Vpcs {
			if vpc.IsDefault != nil && *vpc.IsDefault {
				defaultVpcId = *vpc.VpcId
			} else if vpc.Tags != nil && len(vpc.Tags) > 0 {
				for _, tag := range vpc.Tags {
					if *tag.Key == "Name" {
						if name1Len > 0 && *tag.Value == name1 {
							log.Print("VPC 1: ", *vpc.VpcId)

							vpc1Id = *vpc.VpcId
						} else if name2Len > 0 && *tag.Value == name2 {
							log.Print("VPC 2: ", *vpc.VpcId)

							vpc2Id = *vpc.VpcId
						}
					}
				}
			}
		}
	}

	return defaultVpcId, vpc1Id, vpc2Id
}

func findVpcPeeringConnection(ec2Svc *ec2.EC2, requesterVpcId string, accepterVpcId string) (requestedPeeringConnection *ec2.VpcPeeringConnection) {
	peeringsResp, err := ec2Svc.DescribeVpcPeeringConnections(&ec2.DescribeVpcPeeringConnectionsInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("requester-vpc-info.vpc-id"),
				Values: aws.StringSlice([]string{requesterVpcId}),
			},
			{
				Name:   aws.String("accepter-vpc-info.vpc-id"),
				Values: aws.StringSlice([]string{accepterVpcId}),
			},
			{
				Name:   aws.String("status-code"),
				Values: aws.StringSlice([]string{"initiating-request", "pending-acceptance", "provisioning", "active"}),
			},
		},
	})
	util.HandleError(err)

	if peeringsResp.VpcPeeringConnections == nil {
		log.Print("No VPC peering connections found")
	} else {
		log.Print("Found: ", len(peeringsResp.VpcPeeringConnections), " VPC peering connections")

		for _, peering := range peeringsResp.VpcPeeringConnections {
			if peering.VpcPeeringConnectionId != nil && len(*peering.VpcPeeringConnectionId) > 0 {
				requestedPeeringConnection = peering
			}
		}
	}

	return requestedPeeringConnection
}

func findInternetGatewayForVpc(ec2Svc *ec2.EC2, vpcName string) (requestedInternetGateway *ec2.InternetGateway) {
	gatewaysResp, err := ec2Svc.DescribeInternetGateways(&ec2.DescribeInternetGatewaysInput{})
	util.HandleError(err)

	if gatewaysResp.InternetGateways == nil {
		log.Print("No internet gateways found")
	} else {
		log.Print("Found: ", len(gatewaysResp.InternetGateways), " internet gateways")

		for _, internetGateway := range gatewaysResp.InternetGateways {
			if internetGateway.InternetGatewayId != nil && len(*internetGateway.InternetGatewayId) > 0 && internetGateway.Tags != nil {
				for _, tag := range internetGateway.Tags {
					if tag.Key != nil && tag.Value != nil && *tag.Key == "Name" && *tag.Value == vpcName {
						requestedInternetGateway = internetGateway
					}
				}
			}
		}
	}

	return requestedInternetGateway
}

func findRouteTableForVpc(ec2Svc *ec2.EC2, vpcId string) *ec2.RouteTable {
	routeTablesResp, err := ec2Svc.DescribeRouteTables(&ec2.DescribeRouteTablesInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("vpc-id"),
				Values: aws.StringSlice([]string{vpcId}),
			},
		},
	})
	util.HandleError(err)

	if routeTablesResp.RouteTables == nil {
		log.Fatal("No route tables found")
	}

	routeTableCount := len(routeTablesResp.RouteTables)

	if routeTableCount < 1 {
		log.Fatal("Empty route tables list")
	} else if routeTableCount > 1 {
		log.Fatal("Found: ", routeTableCount, " route tables for VPC: ", vpcId)
	}

	return routeTablesResp.RouteTables[0]
}

func findSubnetsInVpc(ec2Svc *ec2.EC2, vpcId string, name1 string, name2 string, name3 string) (subnetId1 string, subnetId2 string, subnetId3 string) {
	name1Len := len(name1)
	name2Len := len(name2)
	name3Len := len(name3)
	subnetsResp, err := ec2Svc.DescribeSubnets(&ec2.DescribeSubnetsInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("vpc-id"),
				Values: aws.StringSlice([]string{vpcId}),
			},
		},
	})
	util.HandleError(err)

	if subnetsResp.Subnets != nil {
		log.Print("Found: ", len(subnetsResp.Subnets), " subnets for VPC: ", vpcId)

		for _, subnet := range subnetsResp.Subnets {
			if subnet.Tags != nil {
				for _, tag := range subnet.Tags {
					if *tag.Key == "Name" {
						if name1Len > 0 && *tag.Value == name1 {
							log.Print("Subnet 1: ", *subnet.SubnetId)

							subnetId1 = *subnet.SubnetId

							break
						} else if name2Len > 0 && *tag.Value == name2 {
							log.Print("Subnet 2: ", *subnet.SubnetId)

							subnetId2 = *subnet.SubnetId

							break
						} else if name3Len > 0 && *tag.Value == name3 {
							log.Print("Subnet 3: ", *subnet.SubnetId)

							subnetId3 = *subnet.SubnetId

							break
						}
					}
				}
			}
		}
	}

	return subnetId1, subnetId2, subnetId3
}

func createVpc(ec2Svc *ec2.EC2, name string, cidrBlock string) (createdVpcId string) {
	log.Print("Creating VPC: ", name, " with CIDR block: ", cidrBlock)

	createVpcResp, err := ec2Svc.CreateVpc(&ec2.CreateVpcInput{
		CidrBlock: aws.String(cidrBlock),
	})
	util.HandleError(err)

	if createVpcResp.Vpc == nil {
		log.Fatal("No VPC created")
	}

	if createVpcResp.Vpc.VpcId == nil {
		log.Fatal("No VPC ID returned")
	}

	createdVpcId = *createVpcResp.Vpc.VpcId

	if len(createdVpcId) < 1 {
		log.Fatal("Empty VPC ID returned")
	}

	err = ec2Svc.WaitUntilVpcAvailable(&ec2.DescribeVpcsInput{
		VpcIds: aws.StringSlice([]string{createdVpcId}),
	})
	util.HandleError(err)

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: aws.StringSlice([]string{createdVpcId}),
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})
	util.HandleError(err)

	return createdVpcId
}

func peerOwnedVpcs(ec2Svc *ec2.EC2, requesterVpcId string, accepterVpcId string) (vpcPeeringConnection *ec2.VpcPeeringConnection) {
	attemptCount := 1

	for {
		vpcPeeringConnection = findVpcPeeringConnection(ec2Svc, requesterVpcId, accepterVpcId)

		if vpcPeeringConnection == nil {
			vpcPeeringConnection = createVpcPeeringConnection(ec2Svc, requesterVpcId, accepterVpcId, AppVpcName+"To"+UtilVpcName)
		}

		log.Print("VPC peering connection status on attempt ", attemptCount, ": ", vpcPeeringConnection.Status)

		if vpcPeeringConnection.Status != nil && *vpcPeeringConnection.Status.Code != "initiating-request" {
			if *vpcPeeringConnection.Status.Code == "pending-acceptance" {
				acceptVpcPeeringConnection(ec2Svc, *vpcPeeringConnection.VpcPeeringConnectionId)
			}

			break
		}

		if attemptCount <= 20 {
			attemptCount++
		} else {
			log.Fatal("Failed to peer requesting VPC: ", requesterVpcId, " to accepting VPC: ", accepterVpcId)
		}

		time.Sleep(500 * time.Millisecond)
	}

	return vpcPeeringConnection
}

func createVpcPeeringConnection(ec2Svc *ec2.EC2, requesterVpcId string, accepterVpcId string, name string) (createdVpcPeeringConnection *ec2.VpcPeeringConnection) {
	log.Print("Creating VPC peering connection - requester VPC: ", requesterVpcId, ", accepter VPC: ", accepterVpcId)

	createPeeringResp, err := ec2Svc.CreateVpcPeeringConnection(&ec2.CreateVpcPeeringConnectionInput{
		VpcId:     aws.String(requesterVpcId),
		PeerVpcId: aws.String(accepterVpcId),
	})
	util.HandleError(err)

	createdVpcPeeringConnection = createPeeringResp.VpcPeeringConnection

	if createdVpcPeeringConnection == nil {
		log.Fatal("No VPC peering connection created")
	}

	if createdVpcPeeringConnection.VpcPeeringConnectionId == nil {
		log.Fatal("No VPC peering connection ID returned")
	}

	if len(*createdVpcPeeringConnection.VpcPeeringConnectionId) < 1 {
		log.Fatal("Empty VPC peering connection ID returned")
	}

	err = ec2Svc.WaitUntilVpcPeeringConnectionExists(&ec2.DescribeVpcPeeringConnectionsInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("requester-vpc-info.vpc-id"),
				Values: aws.StringSlice([]string{requesterVpcId}),
			},
			{
				Name:   aws.String("accepter-vpc-info.vpc-id"),
				Values: aws.StringSlice([]string{accepterVpcId}),
			},
		},
	})
	util.HandleError(err)

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{createdVpcPeeringConnection.VpcPeeringConnectionId},
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})
	util.HandleError(err)

	return createdVpcPeeringConnection
}

func acceptVpcPeeringConnection(ec2Svc *ec2.EC2, vpcPeeringConnectionId string) {
	log.Print("Accepting VPC peering connection: ", vpcPeeringConnectionId)

	acceptResp, err := ec2Svc.AcceptVpcPeeringConnection(&ec2.AcceptVpcPeeringConnectionInput{
		VpcPeeringConnectionId: aws.String(vpcPeeringConnectionId),
	})
	util.HandleError(err)

	if acceptResp.VpcPeeringConnection == nil {
		log.Fatal("No VPC peering connection accepted")
	}

	if acceptResp.VpcPeeringConnection.VpcPeeringConnectionId == nil {
		log.Fatal("No VPC ID returned for accepted peering connection")
	}

	err = ec2Svc.WaitUntilVpcPeeringConnectionExists(&ec2.DescribeVpcPeeringConnectionsInput{
		VpcPeeringConnectionIds: []*string{acceptResp.VpcPeeringConnection.VpcPeeringConnectionId},
	})
	util.HandleError(err)
}

func createInternetGateway(ec2Svc *ec2.EC2, vpcName string) (createdInternetGateway *ec2.InternetGateway) {
	log.Print("Creating internet gateway for VPC: ", vpcName)

	createGatewayResp, err := ec2Svc.CreateInternetGateway(&ec2.CreateInternetGatewayInput{})
	util.HandleError(err)

	if createGatewayResp.InternetGateway == nil {
		log.Fatal("No internet gateway created")
	}

	createdInternetGateway = createGatewayResp.InternetGateway

	if createdInternetGateway.InternetGatewayId == nil {
		log.Fatal("No internet gateway ID returned")
	}

	internetGatewayId := *createdInternetGateway.InternetGatewayId

	if len(internetGatewayId) < 1 {
		log.Fatal("Empty internet gateway ID returned")
	}

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: aws.StringSlice([]string{internetGatewayId}),
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(vpcName),
			},
		},
	})
	util.HandleError(err)

	return createdInternetGateway
}

func createSubnet(ec2Svc *ec2.EC2, name string, cidrBlock string, zoneId string, vpcId string) (createdSubnetId string) {
	log.Print("Creating subnet: ", name, " with CIDR block: ", cidrBlock)

	createSubnetResp, err := ec2Svc.CreateSubnet(&ec2.CreateSubnetInput{
		AvailabilityZoneId: aws.String(zoneId),
		CidrBlock:          aws.String(cidrBlock),
		VpcId:              aws.String(vpcId),
	})
	util.HandleError(err)

	if createSubnetResp.Subnet == nil {
		log.Fatal("No subnet created")
	}

	if createSubnetResp.Subnet.SubnetId == nil {
		log.Fatal("No subnet ID returned")
	}

	createdSubnetId = *createSubnetResp.Subnet.SubnetId

	if len(createdSubnetId) < 1 {
		log.Fatal("Empty subnet ID returned")
	}

	err = ec2Svc.WaitUntilSubnetAvailable(&ec2.DescribeSubnetsInput{
		SubnetIds: aws.StringSlice([]string{createdSubnetId}),
	})
	util.HandleError(err)

	_, err = ec2Svc.CreateTags(&ec2.CreateTagsInput{
		Resources: aws.StringSlice([]string{createdSubnetId}),
		Tags: []*ec2.Tag{
			{
				Key:   aws.String("Name"),
				Value: aws.String(name),
			},
		},
	})
	util.HandleError(err)

	return createdSubnetId
}

func associateInternetGatewayWithVpc(ec2Svc *ec2.EC2, internetGateway *ec2.InternetGateway, vpcId string) {
	if internetGateway.InternetGatewayId == nil {
		log.Fatal("Tried to associate internet gateway with no ID")
	}

	internetGatewayId := *internetGateway.InternetGatewayId

	if internetGateway.Attachments != nil {
		for _, attachment := range internetGateway.Attachments {
			if attachment.VpcId != nil && *attachment.VpcId == vpcId {
				if attachment.State == nil {
					log.Fatal("Internet gateway: ", internetGatewayId, ", has no attachment state")
				}

				log.Print("Internet gateway: ", internetGatewayId, " has attachment state: ", *attachment.State)

				return
			}
		}
	}

	log.Print("Associating internet gateway: ", internetGatewayId, " with vpc: ", vpcId)

	_, err := ec2Svc.AttachInternetGateway(&ec2.AttachInternetGatewayInput{
		InternetGatewayId: aws.String(internetGatewayId),
		VpcId:             aws.String(vpcId),
	})
	util.HandleError(err)
}

func associateSubnetWithRouteTable(ec2Svc *ec2.EC2, subnetId string, routeTable *ec2.RouteTable) {
	if routeTable.RouteTableId == nil {
		log.Fatal("Tried to associate route table with no ID")
	}

	routeTableId := *routeTable.RouteTableId

	if routeTable.Associations != nil && len(routeTable.Associations) > 0 {
		for _, association := range routeTable.Associations {
			if association.SubnetId != nil && *association.SubnetId == subnetId {
				return
			}
		}
	}

	log.Print("Associating subnet: ", subnetId, " with route table: ", routeTableId)

	_, err := ec2Svc.AssociateRouteTable(&ec2.AssociateRouteTableInput{
		RouteTableId: aws.String(routeTableId),
		SubnetId:     aws.String(subnetId),
	})
	util.HandleError(err)
}

func routeToInternetGateway(ec2Svc *ec2.EC2, routeTable *ec2.RouteTable, internetGateway *ec2.InternetGateway, cidrBlock string) {
	if routeTable.Routes != nil {
		for _, route := range routeTable.Routes {
			if route.GatewayId != nil && *route.GatewayId == *internetGateway.InternetGatewayId && route.DestinationCidrBlock != nil && *route.DestinationCidrBlock == cidrBlock {
				return
			}
		}
	}

	_, err := ec2Svc.CreateRoute(&ec2.CreateRouteInput{
		RouteTableId:         routeTable.RouteTableId,
		GatewayId:            internetGateway.InternetGatewayId,
		DestinationCidrBlock: aws.String(cidrBlock),
	})
	util.HandleError(err)
}

func routeToVpcPeeringConnection(ec2Svc *ec2.EC2, routeTable *ec2.RouteTable, vpcPeeringConnection *ec2.VpcPeeringConnection, cidrBlock string) {
	if routeTable.Routes != nil {
		for _, route := range routeTable.Routes {
			if route.VpcPeeringConnectionId != nil && *route.VpcPeeringConnectionId == *vpcPeeringConnection.VpcPeeringConnectionId && route.DestinationCidrBlock != nil && *route.DestinationCidrBlock == cidrBlock {
				return
			}
		}
	}

	_, err := ec2Svc.CreateRoute(&ec2.CreateRouteInput{
		RouteTableId:           routeTable.RouteTableId,
		VpcPeeringConnectionId: vpcPeeringConnection.VpcPeeringConnectionId,
		DestinationCidrBlock:   aws.String(cidrBlock),
	})
	util.HandleError(err)
}
