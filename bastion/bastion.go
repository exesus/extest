package bastion

import (
	"exesus.com/extest/ami"
	"exesus.com/extest/config"
	"exesus.com/extest/util"
	"exesus.com/extest/vpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
)

const AddressName = "EXBastionAddr"
const InstanceName = "EXBastion01"
const InstanceType = "t2.micro"
const SecurityGroupName = "EXBastion"
const SecurityGroupDesc = "Allows access to bastion servers"

var SecurityGroup *ec2.SecurityGroup

func Configure() {
	SecurityGroup = util.FindSecurityGroupInVpc(config.Ec2Svc, SecurityGroupName, vpc.UtilVpcId)

	if SecurityGroup == nil {
		SecurityGroup = util.CreateSecurityGroupInVpc(config.Ec2Svc, SecurityGroupName, SecurityGroupDesc, vpc.UtilVpcId)
	}

	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, SecurityGroup, "tcp", 22, 22)
	util.AuthorizeIngressFromCidr(config.Ec2Svc, SecurityGroup, "50.125.118.38/32", "tcp", 22, 22)

	bastionAddress := util.FindAddressByName(config.Ec2Svc, AddressName)

	if bastionAddress == nil {
		bastionAddress = util.AllocateAddress(config.Ec2Svc, AddressName)
	}

	bastionInstance := util.FindInstanceByName(config.Ec2Svc, vpc.BastionZoneASubnetId, InstanceName)

	if bastionInstance == nil {
		bastionInstance = util.CreateInstance(
			config.Ec2Svc, InstanceName, true, nil, InstanceType,
			&ec2.InstanceNetworkInterfaceSpecification{
				AssociatePublicIpAddress: aws.Bool(true),
				DeleteOnTermination:      aws.Bool(true),
				SubnetId:                 aws.String(vpc.BastionZoneASubnetId),
				Groups:                   []*string{SecurityGroup.GroupId},
				DeviceIndex:              aws.Int64(0),
			},
			20, vpc.DefaultKeyName, ami.CentosImageId,
			"",
		)
	}

	if bastionAddress.InstanceId == nil || len(*bastionAddress.InstanceId) < 1 || *bastionAddress.InstanceId != *bastionInstance.InstanceId {
		util.AssociateAddressWithInstance(config.Ec2Svc, bastionAddress, bastionInstance)
	}
}
