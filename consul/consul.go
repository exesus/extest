package consul

import (
	"bytes"
	"exesus.com/extest/ami"
	"exesus.com/extest/config"
	"exesus.com/extest/proxy"
	"exesus.com/extest/util"
	"exesus.com/extest/vpc"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
	"strings"
	"text/template"
)

const IamInstanceProfileName = "ConsulServerProfile"
const InstanceType = "t2.medium"
const InstanceNameZoneA = "EXConsul01"
const RetryJoinTagName = "ConsulEnv"
const Version = "1.4.0"

const s3MasterTokenPath = "/consul/tokens/master"

var MasterToken string

func Configure() {
	masterResp, err := config.S3Svc.GetObject(&s3.GetObjectInput{
		Bucket: config.Bucket.Name,
		Key:    aws.String(s3MasterTokenPath),
	})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == s3.ErrCodeNoSuchKey {
			// ignore
		} else {
			util.HandleError(err)
		}
	}

	if masterResp.Body != nil {
		var bodyBuffer bytes.Buffer

		_, err := bodyBuffer.ReadFrom(masterResp.Body)
		util.HandleError(err)

		MasterToken = bodyBuffer.String()
	}

	if len(MasterToken) < 1 {
		randomUuid, err := uuid.NewRandom()
		util.HandleError(err)

		MasterToken = randomUuid.String()

		_, err = config.S3Svc.PutObject(&s3.PutObjectInput{
			Bucket:               config.Bucket.Name,
			Key:                  aws.String(s3MasterTokenPath),
			Body:                 strings.NewReader(MasterToken),
			ServerSideEncryption: aws.String("AES256"),
		})
		util.HandleError(err)
	}

	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8300, 8300)
	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8301, 8301)
	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8500, 8500)
	util.AuthorizeIngressFromSecurityGroup(config.Ec2Svc, vpc.AppSecurityGroup, vpc.AppSecurityGroup, "tcp", 8501, 8501)

	util.EnsureIamPolicyAttachedToRole(config.IamSvc, config.IamPathPrefix, config.ConsulServerRole, config.DiscoverInstancesPolicy)

	iamInstanceProfile := util.FindOrCreateIamInstanceProfileWithRoleAssociation(config.IamSvc, config.IamPathPrefix, IamInstanceProfileName, config.ConsulServerRole)
	aZoneInstance := util.FindInstanceByName(config.Ec2Svc, vpc.AppZoneASubnetId, InstanceNameZoneA)

	if aZoneInstance == nil {
		userDataVars := struct {
			ConsulVersion, EnvironmentName, MasterToken, NoProxy, ProxyAddr, RetryJoinTag string
		}{
			ConsulVersion:   Version,
			EnvironmentName: config.EnvironmentName,
			MasterToken:     MasterToken,
			NoProxy:         "169.254.169.254",
			ProxyAddr:       *proxy.NetworkInterface.PrivateIpAddress,
			RetryJoinTag:    RetryJoinTagName,
		}
		userDataTemplate, err := template.New("ConsulUserData").Parse(`#!/bin/bash
			proxy_available=0
			attempts=0

			while [ "${proxy_available}" = "0" ];
			do
				(timeout 1 bash -c "echo > /dev/tcp/{{.ProxyAddr}}/3128") >/dev/null 2>&1
				result=$?
				attempts=$((attempts+1))

				if [ "${result}" = "0" ];
				then
					echo "Proxy port open on attempt ${attempts}"
					proxy_available=1
				elif [ "${attempts}" -lt 90 ];
				then
					>&2 echo "Proxy port not yet open on attempt ${attempts} - sleeping"
					sleep 5
				else
					>&2 echo "Proxy port not open after ${attempts} attempts - giving up"
					exit 1
				fi
			done

			grep "{{.ProxyAddr}}" /etc/environment || tee -a /etc/environment > /dev/null << __EOF__
http_proxy=http://{{.ProxyAddr}}:3128/
https_proxy=http://{{.ProxyAddr}}:3128/
no_proxy={{.NoProxy}}
__EOF__
			[ -f /etc/profile.d/proxy.sh ] || tee /etc/profile.d/proxy.sh > /dev/null << __EOF__
http_proxy=http://{{.ProxyAddr}}:3128/
https_proxy=http://{{.ProxyAddr}}:3128/
no_proxy={{.NoProxy}}
export http_proxy https_proxy no_proxy
__EOF__

			. /etc/profile.d/proxy.sh

			[ -f /usr/local/bin/consul-{{.ConsulVersion}} ] || { \
				mkdir -p /usr/local/bin && \
				curl --compressed https://releases.hashicorp.com/consul/{{.ConsulVersion}}/consul_{{.ConsulVersion}}_linux_amd64.zip > /home/centos/consul_{{.ConsulVersion}}.zip && \
				python -c 'from zipfile import ZipFile; zf = ZipFile("/home/centos/consul_{{.ConsulVersion}}.zip", "r"); zf.extractall("/home/centos"); zf.close()' && \
				mv -f /home/centos/consul /usr/local/bin/consul-{{.ConsulVersion}} && \
				chmod a+x /usr/local/bin/consul-{{.ConsulVersion}} && \
				chown root:root /usr/local/bin/consul-{{.ConsulVersion}}
			}

            ln -sf /usr/local/bin/consul-{{.ConsulVersion}} /usr/local/bin/consul
			useradd consul
			rm -rf /opt/consul
			mkdir -p /opt/consul
			chown -R consul:consul /opt/consul
			mkdir -p /etc/consul
            touch /etc/consul/config.json
            chown -R consul:consul /etc/consul
            chmod 0700 /etc/consul
            chown 0600 /etc/consul/config.json
			tee /etc/consul/config.json > /dev/null << __EOF__
{
  "acl": {
    "enabled": true,
    "default_policy": "deny",
    "down_policy": "deny",
    "tokens": {
      "default": "{{.MasterToken}}",
      "master": "{{.MasterToken}}"
    }
  },
  "bootstrap": true,
  "datacenter": "{{.EnvironmentName}}",
  "data_dir": "/opt/consul",
  "disable_remote_exec": true,
  "disable_update_check": true,
  "leave_on_terminate": true,
  "log_level": "INFO",
  "retry_join": [
    "provider=aws tag_key={{.RetryJoinTag}} tag_value={{.EnvironmentName}}"
  ],
  "server": true
}
__EOF__

			tee /etc/systemd/system/consul.service > /dev/null << __EOF__
[Unit]
Description="HashiCorp Consul"
Documentation=https://www.consul.io/
Requires=network-online.target
After=network-online.target
ConditionFileNotEmpty=/etc/consul/config.json

[Service]
Environment=http_proxy=http://{{.ProxyAddr}}:3128/
Environment=https_proxy=http://{{.ProxyAddr}}:3128/
Environment=no_proxy={{.NoProxy}}
User=consul
Group=consul
ExecStart=/usr/local/bin/consul agent -config-dir=/etc/consul/
ExecReload=/usr/local/bin/consul reload
KillMode=process
Restart=on-failure
RestartSec=42s
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
__EOF__

			systemctl enable consul
			systemctl start consul
		`)
		util.HandleError(err)

		var userDataBytes bytes.Buffer

		err = userDataTemplate.Execute(&userDataBytes, userDataVars)
		util.HandleError(err)

		aZoneInstance = util.CreateInstance(
			config.Ec2Svc, InstanceNameZoneA, false, iamInstanceProfile, InstanceType,
			&ec2.InstanceNetworkInterfaceSpecification{
				AssociatePublicIpAddress: aws.Bool(false),
				DeleteOnTermination:      aws.Bool(true),
				SubnetId:                 aws.String(vpc.AppZoneASubnetId),
				Groups:                   []*string{vpc.AppSecurityGroup.GroupId},
				DeviceIndex:              aws.Int64(0),
			},
			60, vpc.DefaultKeyName, ami.CentosImageId,
			userDataBytes.String(),
		)

		_, err = config.Ec2Svc.CreateTags(&ec2.CreateTagsInput{
			Resources: []*string{aZoneInstance.InstanceId},
			Tags: []*ec2.Tag{
				{
					Key:   aws.String(RetryJoinTagName),
					Value: aws.String(config.EnvironmentName),
				},
			},
		})
		util.HandleError(err)
	}
}
