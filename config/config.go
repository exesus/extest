package config

import (
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/budgets"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/iam"
	"github.com/aws/aws-sdk-go/service/kms"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sts"
	"log"
	"strings"
)

const EnvironmentName = "exotest"

var AwsAccount string
var AwsPartition string
var AwsRegion string
var AwsSession *session.Session
var BudgetSvc *budgets.Budgets
var Ec2Svc *ec2.EC2
var IamSvc *iam.IAM
var KmsSvc *kms.KMS
var S3Svc *s3.S3
var StsSvc *sts.STS

func init() {
	AwsSession = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	BudgetSvc = budgets.New(AwsSession)
	Ec2Svc = ec2.New(AwsSession)
	IamSvc = iam.New(AwsSession)
	KmsSvc = kms.New(AwsSession)
	S3Svc = s3.New(AwsSession)
	StsSvc = sts.New(AwsSession)

	AwsRegion = findRegion()
	AwsAccount, AwsPartition = extractIdentity()
}

func Configure() {
	configureIam()
	configureStore()
}

func findRegion() string {
	if AwsSession.Config == nil {
		log.Fatal("AWS session has no config")
	}

	if AwsSession.Config.Region == nil {
		log.Fatal("AWS session config has no region")
	}

	if len(*AwsSession.Config.Region) < 1 {
		log.Fatal("Empty region in AWS session config")
	}

	log.Println("Region:", *AwsSession.Config.Region)

	return *AwsSession.Config.Region
}

func extractIdentity() (account string, partition string) {
	identityResp, err := StsSvc.GetCallerIdentity(&sts.GetCallerIdentityInput{})
	util.HandleError(err)

	if identityResp.Account == nil {
		log.Fatal("No account ID returned")
	}

	account = *identityResp.Account

	if len(account) < 1 {
		log.Fatal("Empty account ID returned")
	}

	log.Println("Account:", account)

	if identityResp.Arn == nil {
		log.Fatal("No ARN returned")
	}

	if len(*identityResp.Arn) < 1 {
		log.Fatal("Empty ARN returned")
	}

	log.Println("ARN:", *identityResp.Arn)

	arnParts := strings.Split(*identityResp.Arn, ":")

	if len(arnParts) < 6 {
		log.Fatal("ARN did not have enough components")
	}

	partition = arnParts[1]

	if len(partition) < 1 {
		log.Fatal("Empty partition in ARN")
	}

	log.Println("Partition:", partition)

	return account, partition
}
