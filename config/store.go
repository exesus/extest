package config

import (
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"strings"
)

var Bucket *s3.Bucket

func configureStore() {
	bucketName := "extest-" + AwsRegion + "-config-" + EnvironmentName
	Bucket = findOrCreateBucketInRegion(bucketName, AwsRegion, "private")
	policyDocument := strings.Replace(strings.Replace(`{
		"Version": "2012-10-17",
		"Id": "ConfigStorePolicy",
		"Statement": [
			{
				"Sid": "DenyIncorrectEncryptionHeader",
				"Effect": "Deny",
				"Principal": "*",
				"Action": "s3:PutObject",
				"Resource": "arn:__PARTITION__:s3:::__BUCKET__/*",
				"Condition": {
					"StringNotEquals": {
						"s3:x-amz-server-side-encryption": "AES256"
					}
				}
			},
			{
				"Sid": "DenyUnEncryptedObjectUploads",
				"Effect": "Deny",
				"Principal": "*",
				"Action": "s3:PutObject",
				"Resource": "arn:__PARTITION__:s3:::__BUCKET__/*",
				"Condition": {
					"Null": {
						"s3:x-amz-server-side-encryption": "true"
					}
				}
			}
		]
	}`, "__PARTITION__", AwsPartition, -1), "__BUCKET__", bucketName, -1)

	ensureBucketPolicy(bucketName, policyDocument)
}

func findOrCreateBucketInRegion(bucketName string, region string, cannedAcl string) (requestedBucket *s3.Bucket) {
	bucketsResp, err := S3Svc.ListBuckets(&s3.ListBucketsInput{})
	util.HandleError(err)
	buckets := bucketsResp.Buckets

	if buckets != nil {
		for _, bucket := range buckets {
			if bucket.Name != nil && *bucket.Name == bucketName {
				requestedBucket = bucket
			}
		}
	}

	if requestedBucket == nil {
		createResp, err := S3Svc.CreateBucket(&s3.CreateBucketInput{
			Bucket: aws.String(bucketName),
			ACL:    aws.String(cannedAcl),
			CreateBucketConfiguration: &s3.CreateBucketConfiguration{
				LocationConstraint: aws.String(region),
			},
		})
		util.HandleError(err)

		if createResp.Location == nil {
			log.Fatal("Created bucket has no location")
		}

		if len(*createResp.Location) < 1 {
			log.Fatal("Created bucket has an empty location")
		}

		requestedBucket = &s3.Bucket{
			Name: aws.String(bucketName),
		}
	}

	return requestedBucket
}

func ensureBucketPolicy(bucketName string, policyDocument string) {
	policyResp, err := S3Svc.GetBucketPolicy(&s3.GetBucketPolicyInput{
		Bucket: aws.String(bucketName),
	})
	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok && awsErr.Code() == "NoSuchBucketPolicy" {
			// ignore
		} else {
			util.HandleError(err)
		}
	}

	if policyResp.Policy == nil || util.StripWhitespace(*policyResp.Policy) != util.StripWhitespace(policyDocument) {
		_, err := S3Svc.PutBucketPolicy(&s3.PutBucketPolicyInput{
			Bucket: aws.String(bucketName),
			Policy: aws.String(policyDocument),
		})
		util.HandleError(err)
	}
}
