package config

import (
	"exesus.com/extest/util"
	"github.com/aws/aws-sdk-go/service/iam"
	"strings"
)

const IamPathPrefix = "/extest-" + EnvironmentName + "/"

var ConsulServerRole *iam.Role
var VaultServerRole *iam.Role
var DiscoverInstancesPolicy *iam.Policy

func configureIam() {
	ConsulServerRole = util.FindOrCreateIamRoleWithEC2AssumePolicy(IamSvc, IamPathPrefix, "ConsulServer")
	VaultServerRole = util.FindOrCreateIamRoleWithEC2AssumePolicy(IamSvc, IamPathPrefix, "VaultServer")
	DiscoverInstancesPolicy = util.FindOrIamCreatePolicy(IamSvc, IamPathPrefix, "DiscoverInstances", strings.Replace(strings.Replace(strings.Replace(`{
		"Version": "2012-10-17",
		"Statement": [
			{
				"Effect": "Allow",
				"Action": "ec2:DescribeInstances",
				"Resource": "*",
				"Condition": {
					"StringEquals": {
						"aws:RequestedRegion": "__REGION__"
					}
				}
			}
		]
	}`, "__PARTITION__", AwsPartition, -1), "__REGION__", AwsRegion, -1), "__ACCOUNT__", AwsAccount, -1))
}
